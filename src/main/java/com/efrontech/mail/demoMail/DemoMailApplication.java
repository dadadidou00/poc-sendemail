package com.efrontech.mail.demoMail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.efrontech.mail.demoMail.controller.MailController;

@SpringBootApplication
public class DemoMailApplication {

	public static void main(String[] args) {
		new MailController().run();
		//SpringApplication.run(DemoMailApplication.class, args);
	}

}
