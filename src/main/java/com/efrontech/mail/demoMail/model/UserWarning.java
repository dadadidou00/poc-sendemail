package com.efrontech.mail.demoMail.model;

import java.util.Date;

import com.google.gson.JsonObject;

//import org.json.JSONObject;

public class UserWarning {
	public String errorType;
	public String message;
	public Boolean display;
	public Date date;
	public String couchId;
	public String crmId;
	public int errorCode;
	public String UserId;
	public String accountId;

	public UserWarning(){
		
	}
	
	public UserWarning(String errorType,String message, boolean display, String crmId, String couchid, int errorCode, String UserId, JsonObject doc){
		this.errorType = errorType;
		this.message = message;
		this.display = display;
		this.date = new Date();
		this.couchId = couchid;
		this.crmId = crmId;
		this.errorCode = errorCode;
		this.UserId = UserId;
		if(doc != null) {
			if(doc.has("Compte__c") && doc.get("Compte__c") != null){
				this.accountId = doc.get("Compte__c").getAsString();
			}
			else if(doc.has("AccountId") && doc.get("AccountId") != null) {
				this.accountId = doc.get("AccountId").getAsString();
			}
		}
		
	}
	
	public JsonObject toJSONObject(){
//		JsonAero json= new JsonAero();
//		json.put("Error Type",errorType);
//		json.put("Message", message);
//		json.put("Display",display);
//		json.put("Date",date.getTime());
//		json.put("CRMId", crmId);
//		json.put("CloudantId", couchId);
//		json.put("RealDate", Tools.currentTime("dd-MM-yyyy HH:mm:ss"));
//		json.put("Code", errorCode);
//		json.put("UserId", UserId);
		return null;
	}
}
